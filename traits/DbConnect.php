<?php
trait DbConnect {

    protected static $db = null;
    protected static $dbHost = 'localhost';
    protected static $dbUser = 'root';
    protected static $dbPassword = 'root';
    protected static $dbName = 'lesson14_homework';

    protected static function setDb() {
        try {
            $pdo = new PDO('mysql:host=' . self::$dbHost . ';dbname=' . self::$dbName, self::$dbUser, self::$dbPassword);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $pdo->exec('SET NAMES "utf8"');
            self::$db = $pdo;
        } catch (Exception $exception) {
            echo '<strong>Failed to connect to database!</strong><br>' . $exception->getMessage();
            die();
        }
    }

    protected static function getDb() {
        if (self::$db === null) {
            self::setDb();
        }
        return self::$db;
    }

}