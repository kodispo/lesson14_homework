<?php
include_once 'traits/DbConnect.php';

abstract class Table {

    use DbConnect;

    protected $attributes = [];
    protected $parametersString = "DEFAULT CHARACTER SET utf8 ENGINE=InnoDB";

    static public function getTableName() {
        return str_replace( 'table', '', strtolower( get_called_class() ) ) . 's';
    }

    public function getParametersString() {
        return $this->parametersString;
    }

    public function createTable() {
        try  {
            self::getDb()->exec('
                CREATE TABLE ' . static::getTableName() . '(
                    ' . static::getAttributesString() . '
                ) ' . static::getParametersString()
            );
        } catch (Exception $exception) {
            echo '<strong>Failed to create table!</strong><br>' . $exception->getMessage();
            die();
        }

    }

    protected function getAttributesString() {
        if (is_array($this->attributes) && !empty($this->attributes)) {
            $fields = [];
            foreach ($this->attributes as $name => $params) {
                $fields[] = $name . ' ' . $params;
            }
            return implode(', ', $fields);
        }
    }

}